import dataNist from './data-nist'
import { levenbergMarquardt as lm1 } from 'ml-levenberg-marquardt'
import { levenbergMarquardt as lm2 } from '@mytskine/curve-fitting'

const benchmarkIterations = 100;
document.querySelector('#benchmark-iterations').innerHTML = `${benchmarkIterations}`;

let remainingTests = dataNist.length;

const tbody = document.querySelector('#results tbody');
const loading = document.querySelector('#results tr.loading td');
for (const tcase of dataNist) {
    setTimeout(
        () => {
            tbody.appendChild(testCase(tcase));
            remainingTests--;
            if (remainingTests > 0) {
                loading.textContent = `Computing, with ${remainingTests} tests remaining…`;
            } else {
                loading.parentNode.remove();
            }
        },
        10
    );
}

function testCase(tcase) {
    const getClass = (e) =>
        (e < 1.01 * tcase.expected.error) ? "success" : "failure";

    const options1 = {initialValues: tcase.options.parameters.initial.slice(0)};
    const r1 = lm1(tcase.data, tcase.modelFunction, options1);
    const startTime1 = performance.now();
    for (let i = 0; i < benchmarkIterations; i++) {
        lm1(tcase.data, tcase.modelFunction, options1);
    }
    const time1 = performance.now() - startTime1;

    const options2 = tcase.options;
    const r2 = lm2(tcase.data, tcase.modelFunction, options2);
    const startTime2 = performance.now();
    for (let i = 0; i < benchmarkIterations; i++) {
        lm2(tcase.data, tcase.modelFunction, tcase.options);
    }
    const time2 = performance.now() - startTime2;

    const tr = document.createElement('tr');
    tr.innerHTML = `
    <th><a href="${tcase.url}">${tcase.name}<a></th>
    <td>${tcase.expected.error.toExponential(1)}</td>
    <td class="${getClass(r1.parameterError)}">${r1.parameterError.toExponential(1)}</td>
    <td>${time1.toFixed(1)}</td>
    <td class="${getClass(r2.error)}">${r2.error.toExponential(1)}</td>
    <td>${time2.toFixed(1)}</td>
    `;
    return tr;
}
